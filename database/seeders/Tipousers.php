<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tipousers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_user')->insert(['tipouser'=>1,'descripcion'=>'Administrador']);
        DB::table('tipo_user')->insert(['tipouser'=>2,'descripcion'=>'Comite']);
        DB::table('tipo_user')->insert(['tipouser'=>3,'descripcion'=>'Socio']);
        DB::table('tipo_user')->insert(['tipouser'=>4,'descripcion'=>'Fiscalización']);
    }
}
