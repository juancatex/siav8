<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->tipouser = '1';
        $user->username = 'user';  //es el admin
        $user->email = 'user@example.com';
        $user->password = bcrypt('secret');
        $user->save();

        $user = new User();
        $user->tipouser = '1';
        $user->username = 'admin';  //es el admin
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secret');
        $user->save();
    }
}
