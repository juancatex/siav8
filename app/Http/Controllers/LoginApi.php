<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use PDF;
class LoginApi extends Controller
{
    public function login(Request $request)
    {
    if (!Auth::attempt($request->only('username', 'password'))) {
    return response()->json([
    'message' => 'Datos invalidos'
               ], 401);
           }
    
    $user = User::where('username', $request['username'])->firstOrFail(); 
    $token = $user->createToken('auth_token')->plainTextToken;
    
    return response()->json([
               'access_token' => $token,
               'token_type' => 'Bearer',
    ]);
    }

    public function gethtml()
    {
        // $verifica=User::all();
        // $dataout='';
        // foreach($verifica as $data) {            
        //     $dataout = $data->activo;           
        // }
        // return $dataout;

        $data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ];
          
        $pdf = PDF::loadView('pdf/test', $data);
    
        return $pdf->stream('itsolutionstuff.pdf');
    }
}
