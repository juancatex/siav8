<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use PDF;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function gethtml()
    {
        // $verifica=User::all();
        // $dataout='';
        // foreach($verifica as $data) {            
        //     $dataout = $data->activo;           
        // }
        // return $dataout;

        $data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ];
          
        $pdf = PDF::loadView('test', $data);
    
        return $pdf->stream('itsolutionstuff.pdf');
    }
    public function gethtml2()
    {
        // $verifica=User::all();
        // $dataout='';
        // foreach($verifica as $data) {            
        //     $dataout = $data->activo;           
        // }
        // return $dataout;

        $data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ];
          
        $pdf = PDF::loadView('test', $data);
    
        return $pdf->stream('itsolutionstuff.pdf');
    }
}
