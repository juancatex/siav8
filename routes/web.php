<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    if(Auth::check()) 
      return view('home');
    else  
      return redirect()->route('login');
});

//Auth::routes(); 
Auth::routes(['reset' => false,'register' => false]);

Route::get('/inicio', [App\Http\Controllers\HomeController::class, 'index'])->name('inicio');
Route::get('/gethtml', [App\Http\Controllers\HomeController::class, 'gethtml']); 
